from os import getcwd, listdir
from os.path import isdir, isfile, join

from loading_display import loading_bar
from PIL import Image
from pytesseract import pytesseract

SUPPORTED_IMG_TYPES = ['png', 'jpg', 'jpeg']


def img_to_string(path: str) -> str:
    img = Image.open(path)
    return pytesseract.image_to_string(img).strip()


def get_file_ending(path: str) -> str:
    return path.split('/')[-1].split('.')[-1]


def is_img(path: str) -> bool:
    return isfile(path) and get_file_ending(path).lower() in SUPPORTED_IMG_TYPES


def get_dirs(path: str) -> list:
    return [d for d in listdir(path) if isdir(join(path, d))]


def get_img_list(path: str) -> list:
    return [d for d in listdir(path) if is_img(join(path, d))]


def save(text: str, path: str):
    try:
        f = open(path, 'w')
        f.write(text)
    except Exception as e:
        print(f'could not extract text from image with path {path}', e)
    finally:
        f.close()


def handle_img(img_path):
    text = img_to_string(img_path)
    if len(text) > 0:
        text_path = f"{img_path.rstrip(get_file_ending(img_path))}txt"
        save(text, text_path)


def traverse(base_dir: str):
    images_in_dir = get_img_list(base_dir)
    for img in images_in_dir:
        loading_bar(images_in_dir.index(img), len(images_in_dir) - 1, bar_length=20, show_percentage=True, label=base_dir)
        handle_img(join(base_dir, img))
    if len(images_in_dir) > 0:
        print()
    for dir in get_dirs(base_dir):
        traverse(join(base_dir, dir))


if '__main__' == __name__:
    traverse(getcwd())
